//
//  CoreSyncTests.swift
//  CoreSyncTests
//
//  Created by innovapost on 2017-11-28.
//  Copyright © 2017 innovapost. All rights reserved.
//

import XCTest
@testable import CoreSync

class CoreSyncTests: XCTestCase {
  
  func testInitilization(){
    let pipeline = Pipeline()
    XCTAssertNotNil(pipeline.ul)
    XCTAssertNotNil(pipeline.dl)
    XCTAssertFalse(pipeline.ul.source.isCancelled)
    XCTAssertFalse(pipeline.dl.source.isCancelled)
    
    pipeline.ul.trigger()
    pipeline.dl.trigger()
  }
}
