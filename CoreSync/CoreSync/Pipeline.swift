//
//  Transportable.swift
//  CoreSync
//
//  Created by innovapost on 2017-11-28.
//  Copyright © 2017 innovapost. All rights reserved.
//

import CoreDataStore


public protocol Triggerable {
  func trigger()
  func resume()
  func suspend()
}

protocol Transportable: Triggerable {
  func transport()
}

fileprivate let ulSource = DispatchSource.makeUserDataAddSource(queue: Pipeline.queue)
fileprivate let dlSource = DispatchSource.makeUserDataAddSource(queue: Pipeline.queue)

extension Triggerable{
  var source: DispatchSourceUserDataAdd {
    return self is UploadPipeline ? ulSource : dlSource
  }
  public func trigger(){ source.add(data: 1) }
  public func resume() { source.resume() }
  public func suspend(){ source.suspend() }
}


public struct Pipeline {
  fileprivate static let queue = DispatchQueue.global(qos: .default)
  public let ul = UploadPipeline()
  public let dl = DownloadPipeline()
  public init() { }
}

public struct UploadPipeline: Transportable , Triggerable {
  
  public init(){
    source.setEventHandler(handler: transport)
    source.resume()
  }

  func transport() { print("UL handler")}
}

public struct DownloadPipeline: Transportable , Triggerable {
  public init(){
    source.setEventHandler(handler: transport)
    source.resume()
  }

  func transport() { print("DL handler")}
}
