//
//  ViewController.swift
//  Sync
//
//  Created by innovapost on 2017-11-27.
//  Copyright © 2017 innovapost. All rights reserved.
//

import UIKit
import iCloudStore
import CoreDataStore
import CoreSync

class ViewController: UIViewController {
  var localeStore: CoreDataStore!
  var iCloud: ICloudStore!
  var pipeline: Pipeline!
  
  @IBAction func onClickDL(_ sender: UIButton) {
    pipeline.dl.trigger()
  }
  
  @IBAction func onClickUL(_ sender: UIButton) {
    pipeline.ul.trigger()
  }
}

